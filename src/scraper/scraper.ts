import * as puppeteer from 'puppeteer';

declare interface ISiteMap {
    links: string[];
    resources: string[];
}

export default class Scraper {
    
    public visited: string[] = [];
    public allUrls: string[] = [];
    public allDomainUrls: string[] = [];
    public allResources: string[] = [];

    constructor() {
    }
    
    /**
     * Provided an input url visit all pages in the same domain
     * and return a JSON sitemap of links and resources
     * 
     * @param {string} url
     * @returns {Promise<ISiteMap>}
     */
    async scrape(url: string): Promise<ISiteMap> {
        const browser: puppeteer.Browser = await puppeteer.launch();
        const page: puppeteer.Page = await browser.newPage();
        
        this.allDomainUrls.push(url);
    
        for (let i = 0; i < this.allDomainUrls.length; i++) {
            await this.goToAndScrapePage(page, this.allDomainUrls[i]);
            console.log('visiting: ' + this.allDomainUrls[i]);
        }
        
        await browser.close();
    
        const siteMap = {
            links: this.allUrls,
            resources: this.allResources
        };
    
        console.log(siteMap);
        return siteMap;
    }

    /**
     * Browse to a specified url, update the tracked links, domain links and resources
     *
     * @param {puppeteer.Page} page
     * @param {string} url
     * @returns
     */
    async goToAndScrapePage(page: puppeteer.Page, url: string) {
    
        // Do not scrape if page has already been visited
        if (this.visited.includes(url)) {
            return;
        }
    
        try {
            await page.goto(url);
        } catch (err) {
            console.error('Unable to reach URL - please check URL format eg - https://wiprodigital.com');
        }
    
        const pageLinks = await this.getLinks(page);
        
        // Track the links on this page and add to the list of all links
        let urls: string[] = [];
        pageLinks.forEach((link) => {
            urls = this.pushIfNotExist(urls, link);
            this.allUrls = this.pushIfNotExist(this.allUrls, link);
        });
    
        // Update the domain links to be scraped
        const pageDomainLinks = this.getDomainUrls(urls, url);
        pageDomainLinks.forEach((link) => {
            this.allDomainUrls = this.pushIfNotExist(this.allDomainUrls, link);
        });
    
        // Update list of resources
        const pageResources = await this.getResources(page, true, true);
        pageResources.forEach((link) => {
            this.allResources = this.pushIfNotExist(this.allResources, link);
        });
    
        // Flag this page as visited so we don't revisit
        this.visited.push(url);
    
        return;
    }
    
    /**
     * Utility function to add a string to an array if that string is not already included
     * 
     * @param {string[]} arr
     * @param {string} str
     * @returns {string[]}
     */
    pushIfNotExist(arr: string[], str: string): string[] {
        const newArr = [...arr];
        if (!newArr.includes(str)) {
            newArr.push(str);
        }
        return newArr;
    }
    
    /**
     * Filter any urls which contain the domain we are scraping
     * TODO - find solution to avoid using the input url as the test string
     *
     * @param {string[]} urls
     * @param {string} url
     * @returns {string[]}
     */
    getDomainUrls(urls: string[], url: string): string[] {
        return urls.filter((u => u.includes(url)));
    }
    
    /**
     * Use puppeteer eval to get a list of anchor href links
     *
     * @param {puppeteer.Page} page
     * @returns {Promise<string[]>}
     */
    async getLinks(page: puppeteer.Page): Promise<string[]> {
        const hrefs: string[] = await page.$$eval('a', as => as.map((a: any) => a.href));
    
        return hrefs;
    }
    
    
    /**
     * Get a list of all the resources on the page
     * Eg. background url images, scripts, etc
     * Uses performace api from pupeteer to find all downloaded resources
     * @param {puppeteer.Page} page
     * @param {boolean} [filterJs=false] Remove .js resources
     * @param {boolean} [filterJs=false] Remove .css resources
     * @returns {Promise<string[]>}
     */
    async getResources(page: puppeteer.Page, filterJs: boolean = false, filterCss: Boolean = false): Promise<string[]> {
        let urls: string[] = await page.evaluate(() => {
            return performance.getEntries()
            .filter(e => e.entryType === 'resource')
            .map(e => e.name);
        });
        
        if (filterJs) {
            urls = urls.filter((url: string) => !url.match(/\.js/));
        }
    
        if (filterCss) {
            urls = urls.filter((url: string) => !url.match(/\.css/));
        }
        
        return urls;
    }
}
