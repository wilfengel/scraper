import Scraper from "./scraper/scraper";

const scraper = new Scraper();
const url = process.argv[2] ? process.argv[2] : 'https://wiprodigital.com';

scraper.scrape(url);