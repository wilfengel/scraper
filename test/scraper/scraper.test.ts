import Scraper from '../../src/scraper/scraper';
import * as puppeteer from 'puppeteer';

let testScraper: any;

describe('scrape()', () => {
    beforeEach(() => {
        testScraper = new Scraper();
    });

    it('should append the provided url to the list', async () => {
        expect(testScraper.allDomainUrls.length).toBe(0);

        const goToSpy = jest.spyOn(testScraper, 'goToAndScrapePage').mockImplementationOnce(async () => {
            return true;
        });

        await testScraper.scrape('https://wiprodigital.com');

        expect(testScraper.allDomainUrls.length).toBe(1);
        expect(goToSpy).toHaveBeenCalledTimes(1);
    });

    it('should visit all pages in the domain', async () => {
        testScraper.allDomainUrls.push('another.test.url');

        const goToSpy = jest.spyOn(testScraper, 'goToAndScrapePage').mockImplementation(async () => {
            return true;
        });

        await testScraper.scrape('https://wiprodigital.com');

        expect(testScraper.allDomainUrls.length).toBe(2);
        expect(goToSpy).toHaveBeenCalledTimes(2);

        goToSpy.mockRestore();
    });
});

describe('goToAndScrapePage()', () => {
    beforeEach(() => {
        testScraper = new Scraper();
    });

    it('should do nothing if the page has been visited', async () => {
        const testUrl = 'https://wiprodigital.com';
        testScraper.visited = [testUrl];

        const page = {
            goto: jest.fn()
        };

        await testScraper.goToAndScrapePage(page, testUrl);
        expect(page.goto).not.toHaveBeenCalled();
    });


    it('should go to the page if it has not been visited', async () => {
        const testUrl = 'https://wiprodigital.com';
        testScraper.visited = [];

        const page = {
            goto: jest.fn()
        };

        const getLinksSpy = jest.spyOn(testScraper, 'getLinks').mockImplementationOnce(async () => {
            return [];
        });

        const getResourcesSpy = jest.spyOn(testScraper, 'getResources').mockImplementationOnce(async () => {
            return [];
        });

        await testScraper.goToAndScrapePage(page, testUrl);
        expect(page.goto).toHaveBeenCalledWith(testUrl);
    });

    it('should add the found links to the lists', async () => {
        const testUrl = 'https://wiprodigital.com';
        testScraper.visited = [];
        testScraper.allUrls = ["1"];
        testScraper.allResources = ["1"];

        const page = {
            goto: jest.fn()
        };

        const getLinksSpy = jest.spyOn(testScraper, 'getLinks').mockImplementationOnce(async () => {
            return ['one', 'two'];
        });

        const getDomainLinksSpy = jest.spyOn(testScraper, 'getDomainUrls').mockImplementationOnce(() => {
            return ['a', 'b'];
        });

        const getResourcesSpy = jest.spyOn(testScraper, 'getResources').mockImplementationOnce(async () => {
            return ['three', 'four'];
        });

        await testScraper.goToAndScrapePage(page, testUrl);
        expect(testScraper.allUrls).toEqual(['1', 'one', 'two']);
        expect(testScraper.allResources).toEqual(['1', 'three', 'four']);
        expect(testScraper.allDomainUrls).toEqual(['a', 'b']);
    });
});


describe('pushIfNotExist()', () => {
    beforeEach(() => {
        testScraper = new Scraper();
    });

    it('should return the original array if the element exists already', async () => {
        const testUrl = 'https://wiprodigital.com';
        const testArray = [testUrl];

        const result = await testScraper.pushIfNotExist(testArray, testUrl);
        expect(result).toEqual(testArray);
    });

    it('should return the the array with the new string added if it didnt exist', async () => {
        const testUrl = 'https://wiprodigital.com';
        const testArray = ['google.test'];
        const expectedResult = [...testArray, testUrl];

        const result = await testScraper.pushIfNotExist(testArray, testUrl);
        expect(result).toEqual(expectedResult);
    });

    it('should not modify the original array', async () => {
        const testUrl = 'https://wiprodigital.com';
        const testArray = ['google.test'];
        const expectedResult = [...testArray, testUrl];

        const result = await testScraper.pushIfNotExist(testArray, testUrl);
        expect(testArray).toEqual(testArray);
    });
});

describe('getDomainUrls()', () => {
    beforeEach(() => {
        testScraper = new Scraper();
    });

    it('should return the array unmodified if there are no matching domains', async () => {
        const testUrl = 'https://wiprodigital.com';
        const testArray = [testUrl, testUrl + '/test'];

        const result = await testScraper.getDomainUrls(testArray, testUrl);
        expect(result).toEqual(testArray);
    });

    it('should return the array with any non domain urls removed', async () => {
        const testUrl = 'https://wiprodigital.com';
        const testArray = [testUrl, testUrl + '/test', 'google.test'];
        const expectedResult = [testUrl, testUrl + '/test']

        const result = await testScraper.getDomainUrls(testArray, testUrl);
        expect(result).toEqual(expectedResult);
    });
});

describe('getLinks()', () => {
    beforeEach(() => {
        testScraper = new Scraper();
    });

    it('should return a list of strings returned by puppeteer.Page.$$eval', async () => {
        const page = {
            $$eval: jest.fn(() => {
                return ['a', 'b'];
            })
        };

        const result = await testScraper.getLinks(page);
        expect(result).toEqual(['a', 'b']);
    });
});

describe('getResources()', () => {
    beforeEach(() => {
        testScraper = new Scraper();
    });

    it('should return a list of strings returned by puppeteer.Page.evaluate', async () => {
        const page = {
            evaluate: jest.fn(() => {
                return ['a', 'b'];
            })
        };

        const result = await testScraper.getResources(page);
        expect(result).toEqual(['a', 'b']);
    });

    it('should filter the .js files if required', async () => {
        const page = {
            evaluate: jest.fn(() => {
                return ['a', 'b.js'];
            })
        };

        const result = await testScraper.getResources(page, true);
        expect(result).toEqual(['a']);
    });

    it('should filter the .css files if required', async () => {
        const page = {
            evaluate: jest.fn(() => {
                return ['a.css', 'b.js'];
            })
        };

        const result = await testScraper.getResources(page, false, true);
        expect(result).toEqual(['b.js']);
    });

    it('should filter both the .js and .css files if required', async () => {
        const page = {
            evaluate: jest.fn(() => {
                return ['a.css', 'b.js', 'c'];
            })
        };

        const result = await testScraper.getResources(page, true, true);
        expect(result).toEqual(['c']);
    });
});